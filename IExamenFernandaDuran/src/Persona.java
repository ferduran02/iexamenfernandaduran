/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author TRABAJOS
 */
public class Persona {
    private String nombre;
    private String Apellidos;
    private int edad;
    private String lugar;

    public Persona(String nombre, String Apellidos, int edad, String lugar, String ocupacion, int telefono) {
        this.nombre = nombre;
        this.Apellidos = Apellidos;
        this.edad = edad;
        this.lugar = lugar;
        this.ocupacion = ocupacion;
        this.telefono = telefono;
    }
    private String ocupacion;
    private int telefono;
    
    public String obtenerNombre(String nombre, String apellido){
       return nombre +" " + Apellidos + " ";
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidos() {
        return Apellidos;
    }

    public void setApellidos(String Apellidos) {
        this.Apellidos = Apellidos;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public String getLugar() {
        return lugar;
    }

    public void setLugar(String lugar) {
        this.lugar = lugar;
    }

    public String getOcupacion() {
        return ocupacion;
    }

    public void setOcupacion(String ocupacion) {
        this.ocupacion = ocupacion;
    }

    public int getTelefono() {
        return telefono;
    }

    public void setTelefono(int telefono) {
        this.telefono = telefono;
    }
    
}
